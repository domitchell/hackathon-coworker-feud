const { app, BrowserWindow, ipcMain } = require('electron')

// load .env file for environment variables.
require('dotenv').config()

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let adminWindow
let feudWindow

function createWindow () {
  feudWindow = new BrowserWindow({height: 900, width: 1600})
  feudWindow.loadFile('feud/feud.html')

  //uncomment if you want to open DevTools by default
  //feudWindow.webContents.openDevTools()

  feudWindow.on('closed', () => {
    feudWindow = null
    if(adminWindow) adminWindow.close()
  })

  adminWindow = new BrowserWindow({height: 900, width: 1600})
  adminWindow.loadFile('admin/admin.html')

  //uncomment if you want DevTools open by default 
  //adminWindow.webContents.openDevTools()

  adminWindow.on('closed', () => {
    adminWindow = null
    if(feudWindow) feudWindow.close()
  })

  ipcMain.on('send-update', (event, data) => {
    feudWindow.webContents.send('recieve-update', data)
    adminWindow.webContents.send('recieve-update', data)
  })

  ipcMain.on('send-action', (event, data) => {
    feudWindow.webContents.send('recieve-action', data)
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)
