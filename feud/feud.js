ipcRenderer = require('electron').ipcRenderer;
$ = require('jquery')

let family1Score = 0;
let family2Score = 0;
let roundScore = 0;
let answers = []

// answers will be an array with 0 <= length <= 8
// answers[x] will have the form {answer: String, points: Number, show: Boolean}
ipcRenderer.on('recieve-update', (event, data) => {
	family1Score = data['family1Score']
  family2Score = data['family2Score']
  roundScore = data['roundScore']
  answers = data['answers']
  updateWindow();
});

ipcRenderer.on('recieve-action', (event, data) => {
  element = document.getElementById(`strike${data.strike}`);
  element.classList.remove('flash')
  // https://css-tricks.com/restart-css-animation/
  void element.offsetWidth
  element.classList.add('flash')
});

function updateWindow() {
  setAnswers()
  setRoundScore()
  setFamily1Score()
  setFamily1Score()
  setFamily2Score()
}

function setAnswers() {
  for(var x = 0; x < 8; x++) {
    var content = '<div></div>'
    if(answers.length > x) {
      if(answers[x].show) {
        content = `<div class='card'><div class='back DBG'><span>${answers[x].answer}</span><b class="LBG">${answers[x].points}</b></div></div>`
      }
      else {
        content = `<div class='card'><div class='front'><span class='DBG'>${x + 1}</span></div></div>`
      }
    }
    $(`#answer-${x}`).html(content)
  }
}

function setRoundScore() {
  $("#boardScore").text(roundScore)
}

function setFamily1Score() {
  $("#team1").text(family1Score)
}

function setFamily2Score() {
  $("#team2").text(family2Score)
}
