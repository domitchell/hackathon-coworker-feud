const ipcRenderer = require('electron').ipcRenderer;
const Papa = require('papaparse')

class GameController {
  constructor() {
    this.family1Score = 0;
    this.family2Score = 0;
    this.roundScore = 0;
    this.roundNumber = 1;
    this.roundMultiplier = 1;
    // questions[x] will look like { question: String, answers: Array }
    // answers will be an array with 0 <= length <= 8
    // answers[x] will have the form {answer: String, points: Number, show: Boolean}
    this.questions = [];
    this.currentQuestionIndex = -1;
  }

  updateProperty(property, value) {
    this[property] = value
    this.updateWindows()
  }

  updateWindows() {
    let gameState = {
      family1Score: this.family1Score,
      family2Score: this.family2Score,
      roundScore: this.roundScore,
      answers: this.currentQuestion().answers,
      question: this.currentQuestion.question,
      roundNumber: this.roundNumber,
      roundMultiplier: this.roundMultiplier,
      questions: this.questions.map(function(question) {
        return question.question
      }),
      currentQuestionIndex: this.currentQuestionIndex
    }

    ipcRenderer.send('send-update', gameState);
  }

  newGame() {
    this.roundScore = 0
    this.roundNumber = 0
    this.family1Score = 0
    this.family2Score = 0
    this.roundMultiplier = 1
    this.nextQuestion()
  }

  answerQuestion(index) {
    if(index >= 0 && index < this.currentQuestion().answers.length) {
      this.currentAnswers()[index].show = true
      this.roundScore += (this.currentQuestion().answers[index].points * this.roundMultiplier)
    }
    this.updateWindows()
  }

  family1WinRound() {
    this.family1Score += this.roundScore
    this.roundScore = 0
    this.updateWindows()
  }

  family2WinRound() {
    this.family2Score += this.roundScore
    this.roundScore = 0
    this.updateWindows()
  }

  parseQuestions(file) {
    this.questions = []
    this.currentQuestionIndex = -1
    Papa.parse(file.path, {
      download: true,
      header: true,
      delimiter: ",",
      complete: (results, file) => {
        var headers = results.meta.fields
        if(!headers.includes("Question") || !headers.includes("Answer") || !headers.includes("Points")) {
          M.toast({html: 'Error Parsing File'})
          M.toast({html: 'CSV must have the headers: Question, Answer, and Points'})
        }
        var questionHash = {}
        var index = 0
        results.data.forEach(answer => {
          if(!questionHash[answer.Question]) {
            questionHash[answer.Question] = {
              question: answer.Question,
              answers: [],
              index: index
            }
            index++
          }
          if(questionHash[answer.Question].answers.length < 8) {
            questionHash[answer.Question].answers.push({
              answer: answer.Answer, points: parseInt(answer.Points), show: false
            })
          }
        })
        Object.keys(questionHash).forEach(key => {
          this.questions[questionHash[key].index] = questionHash[key]
        })
        this.newGame()
      }
    })
  }

  nextQuestion() {
    this.currentQuestionIndex++
    if(this.currentQuestionIndex >= this.questions.length) {
      M.toast({html: 'Out of Questions'})
    }
    this.roundReset()
    this.cleanAnswers()
    this.updateWindows()
  }

  selectQuestion(index) {
    this.currentQuestionIndex = index
    this.cleanAnswers()
    this.roundScore = 0
    this.updateWindows()
  }

  revealAllQuestions() {
    this.currentAnswers().forEach(answer => {
      answer.show = true
    })
    this.updateWindows()
  }

  // Helper Functions
  // TODO: make private

  currentAnswers() {
    return this.currentQuestion().answers
  }

  currentQuestion() {
    if( this.currentQuestionIndex >= this.questions.length || this.currentQuestionIndex < 0) return { question: '', answers: [] }
    return this.questions[this.currentQuestionIndex]
  }

  cleanAnswers() {
    this.currentQuestion().answers.forEach(answer => {
      answer.show = false
    })
  }

  roundReset() {
    this.roundNumber++
    this.roundScore = 0
    this.roundMultiplier = this.newRoundMultiplier()
  }

  newRoundMultiplier() {
    if(this.roundNumber > 3) {
      return 3
    }
    else if(this.roundNumber ==  3) {
      return 2
    }
    else {
      return 1
    }
  }
}

module.exports = GameController;
