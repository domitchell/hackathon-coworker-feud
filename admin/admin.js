GameController = require('../gameController.js');
const {ipcRenderer} = require('electron');
const {dialog} = require('electron').remote;
debounce = require('debounce');
$ = require('jquery');

let controller = new GameController();

let viewInputs = [
  'family1Score',
  'family2Score',
  'roundScore',
  'roundNumber',
  'roundMultiplier'
]

let actionButtons = [
  'family1WinRound',
  'family2WinRound',
  'nextQuestion',
  'revealAllQuestions',
  'newGame'
]

ipcRenderer.on('recieve-update', (event, data) => {
  updateWindow(data)
});

// Input Actions
viewInputs.forEach(input => {
  function updateProp(event) {
    controller.updateProperty(input, parseInt(event.target.value))
  }

  document.getElementById(input).oninput = debounce(updateProp, 300)
})

document.getElementById('question-select-options').addEventListener('change', event => {
  controller.selectQuestion(parseInt(event.target.value))
});

// Button Actions

actionButtons.forEach(button => {
  document.getElementById(button).addEventListener('click', event => {
  	controller[button]()
  });
})

$('.play-sound').click(event => {
  document.getElementById(event.target.dataset.sound).play()
})

$('.strike').click(event => {
  ipcRenderer.send(
    'send-action', { strike: event.target.dataset.strikeNumber }
  )
})

document.getElementById('parseQuestions').addEventListener('change', event => {
  controller.parseQuestions(event.target.files[0])
});

// Helper Functions

function updateWindow(data) {
  viewInputs.forEach((input) => {
    $(`#${input}`).val(data[input])
  })
  createAnswerList(data.answers)
  createQuestionList(data.questions, data.currentQuestionIndex)
}

function createQuestionList(questions, selectedQuestionIndex) {
  var element = $('#question-select-options')
  element.empty();
  if(selectedQuestionIndex < 0 || selectedQuestionIndex >= questions.length) {
    element.append('<option value="" disabled selected>Choose Question</option>')
  }
  questions.forEach((question, index) => {
    if(index == selectedQuestionIndex) {
      element.append(`<option value="${index}" selected>${question}</option>`)
    }
    else {
      element.append(`<option value="${index}">${question}</option>`)
    }
  })
  M.FormSelect.init(document.getElementById('question-select-options'));
}

function createAnswerList(answers) {
  $('#answer-table-body').empty()
  answers.forEach((answer, index) => {
    var content = ''
    if(answer.show){
      content = `<tr><td>${answer.answer}</td><td>${answer.points}</td><td></td></tr>`
    }
    else {
      content = `<tr><td>${answer.answer}</td><td>${answer.points}</td><td><a class="btn show-answer" data-answer='${index}'>Show</a></td></tr>`
    }
    $('#answer-table-body').append(content)
  })

  $('.show-answer').click(event => {
    controller.answerQuestion(parseInt(event.target.dataset.answer))
    document.getElementById('answerreveal').play()
  })
}

//materialize initialization
document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems);
});
